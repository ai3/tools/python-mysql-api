import json
import optparse
import os
import re
import ssl
import subprocess
import sys
from functools import wraps
from flask import Flask, request, abort, make_response

import pymysql
import pymysql.cursors

from .sso_api import sso_api_auth_required, init_sso
from .tls_auth import tls_auth, init_tls_auth, PeerCertWSGIRequestHandler


app = Flask(__name__)
app.config['TLS_AUTH_ACLS'] = [
    ('/', 'client'),
]


def to_json(fn):
    """Encode response as JSON."""
    @wraps(fn)
    def _json_wrapper(*args, **kwargs):
        result = fn(*args, **kwargs)
        resp = make_response(json.dumps(result))
        resp.headers['Content-Type'] = 'application/json'
        return resp
    return _json_wrapper


def _random_password():
    return os.urandom(12).encode('base64').rstrip('=\n')


def _connect(host='localhost', dbname=None,
            cursor_class=None, defaults_file=None):
    args = {'host': host,
            'read_default_file': defaults_file,
            'init_command': 'SET NAMES utf8'}
    if dbname:
        args['db'] = dbname
    c = pymysql.connect(**args)
    return c.cursor(cursor_class)


@app.route('/api/pwreset', methods=('POST',))
@tls_auth
@sso_api_auth_required
@to_json
def reset_password():
    db_user = request.json['db_user']

    password = _random_password()
    conn = _connect(
        dbname='mysql',
        host=app.config.get('MYSQL_HOST', 'localhost'),
        defaults_file=app.config.get('MYSQL_DEFAULTS_FILE'),
    )

    conn.execute('SET PASSWORD FOR ?@localhost = PASSWORD(?)',
                 db_user, password)
    app.logger.info('mysql password reset for db_user=%s on behalf of %s',
                    db_user, g.current_user)

    return {
        'password': password,
    }


### SSL server with the right parameters.

def serve_ssl(app, host='localhost', port=3000, **kwargs):
    # Create a validating SSLContext.
    ssl_ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    ssl_ctx.set_ciphers('ECDHE-ECDSA-AES256-GCM-SHA384')
    ssl_ctx.load_cert_chain(certfile=app.config['SSL_CERT'],
                            keyfile=app.config['SSL_KEY'])
    ssl_ctx.load_verify_locations(cafile=app.config['SSL_CA'])
    ssl_ctx.verify_mode = ssl.CERT_REQUIRED

    app.run(
        host, port,
        ssl_context=ssl_ctx,
        request_handler=PeerCertWSGIRequestHandler,
        **kwargs
    )


def main():
    parser = optparse.OptionParser()
    parser.add_option('--config', default='/etc/mysql_api.conf')
    parser.add_option('--port', type='int', default=5778)
    parser.add_option('--addr', default='0.0.0.0')
    opts, args = parser.parse_args()
    if args:
        parser.error('Too many arguments')

    app.config.from_pyfile(opts.config)
    init_sso(app)
    init_tls_auth(app)
    serve_ssl(app, host=opts.addr, port=opts.port)


if __name__ == '__main__':
    main()
