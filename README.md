Simple API server that provides our user panel with the ability to
reset passwords of MySQL databases.

It's just an SSO-protected HTTP endpoint with JSON
requests/responses. There is no attempt to verify that the SSO user
actually owns the database user whose password should be reset.

