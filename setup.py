#!/usr/bin/python

from setuptools import setup, find_packages


setup(
    name="mysql-api",
    version="0.1",
    description="Internal Mailman API server",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai3/python-mysql-api",
    install_requires=["Flask",
                      "PyMySQL",
                      "sso"],
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "python-mysql-api-server = mysql_api.mysql_api:main",
        ],
    },
)

