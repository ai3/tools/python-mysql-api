FROM debian:buster AS build

RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends \
        python3 python3-dev python3-pip python3-setuptools python3-wheel git build-essential

ADD . /src
WORKDIR /src
RUN mkdir -p dist; pip3 wheel -r requirements.txt -w dist
RUN python3 setup.py bdist_wheel

FROM debian:buster

RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends \
        python3 python3-pip python3-setuptools python3-wheel \
    && apt-get clean \
    && rm -fr /var/lib/apt/lists/*

COPY --from=build /src/dist/*.whl /tmp/wheels/
RUN cd /tmp/wheels && pip3 install *.whl && rm -fr /tmp/wheels

ENTRYPOINT ["/usr/bin/python-mysql-api-server"]

